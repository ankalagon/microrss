//
//  MGBAppDelegate.m
//  MicroRSS
//
//  Created by Grzesiek on 05/24/13.
//

#import <ASIHTTPRequest/ASIHTTPRequest.h>
#import "MGBAppDelegate.h"
#import "CNSplitView.h"
#import "TinyTinyRSS.h"
#import "PXSourceList.h"
#import "MicroRSSDatabase.h"
#import "MicroRSSInitialMigration.h"
#import "FmdbMigrationManager.h"
#import "MicroRSSServiceTable.h"
#import "MGBArticleViewController.h"
#import "TinyTinyRSSFeed.h"
#import "MGBErrorWindowController.h"
#import "MASPreferencesWindowController.h"
#import "MGBAccountsPrefViewController.h"
#import "MGBAddServiceWindowController.h"
#import "MGBSubscribeWindowController.h"
#import "TinyTinyRSSArticle.h"
#import "NSOutlineView+SelectItem.h"
#import "MicroRSSMigrationV2.h"

@interface MGBAppDelegate ()
// Wczytuje dane dostępnych serwisów z bazy
- (void)loadServicesFromDB;

// Konfiguracja widoku z podziałem
- (void)setupSplitView;

// Konfiguruje widok artykułów
- (void)setupArticleView;

// Dodaje liczbę nieprzeczytanych artykułów jako ikonkę w dock'u
- (void)updateDockBadge;

// Wyświetla panel z listą błędów
- (IBAction)showErrorsPanel:(id)sender;

// Metoda wywoływana po zakończeniu aktualizacji wszystkich serwisów
- (void)allDataUpdated;

// Tablica zawierająca dostępne serwisy
@property(nonatomic, retain) NSMutableArray *services;

// Widok odpowiedzialny za wyświetlanie artykułów
@property (nonatomic, retain) MGBArticleViewController *articleView;

// Guzik z ostrzeżeniem o błędach
@property (nonatomic, retain) CNSplitViewToolbarButton *warningButton;

// Tablica z błędami
@property (nonatomic, retain) NSMutableArray *warningsArray;

// Kontroler odpowiedzialny za wyświetlanie błędów
@property (nonatomic, retain) MGBErrorWindowController *errorWindowController;

// Timer odpowiedzialny za cykliczne odświeżanie listy artykułów
@property (nonatomic, retain) NSTimer *updateTimer;

// Kontroler z formularzem dodawania nowego konta
@property (nonatomic, retain) MGBAddServiceWindowController *addServiceWindowController;

// Kontroler z formularzem subskrybcji do feed'a
@property (nonatomic, retain) MGBSubscribeWindowController *subscribeWindowController;

@end

@implementation MGBAppDelegate
// ---------------------------------------------------------------------------------------------------------------------
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [self setupSplitView];
    [self setupArticleView];

    [ASIHTTPRequest setDefaultTimeOutSeconds:60];

    [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mainWindowClosed:) name:NSWindowWillCloseNotification object:self.window];

    [[NSAppleEventManager sharedAppleEventManager] setEventHandler:self andSelector:@selector(getUrl:withReplyEvent:) forEventClass:kInternetEventClass andEventID:kAEGetURL];

    self.warningsArray = [[NSMutableArray alloc] init];

    // Migracja bazy danych do najnowszej wersji
    NSArray *migrations = @[
        [MicroRSSInitialMigration migration],
        [MicroRSSMigrationV2 migration]
    ];
    [FmdbMigrationManager executeForDatabasePath:[MicroRSSDatabase databasePathString] withMigrations:migrations];

    [self.progressBar startAnimation:self];

    // Inicjalizacja listy serwisów
    [self loadServicesFromDB];
    if (self.services.count == 0) {
        // brak serwisów w bazie - trzeba otworzyć okno dodawania nowego
        [self.progressBar stopAnimation:self];
        self.progressBarHeight.constant = 0;

        if (!self.addServiceWindowController) {
            MGBAddServiceWindowController *addController = [[MGBAddServiceWindowController alloc] initWithWindowNibName:@"MGBAddServiceWindow"];

            self.addServiceWindowController = addController;
        }

        [NSApp beginSheet:self.addServiceWindowController.window modalForWindow:self.window
            modalDelegate:self
           didEndSelector:@selector(addServiceSheetDidEnd:returnCode:contextInfo:)
              contextInfo:nil];
    } else {
        for (TinyTinyRSS *rss in self.services) {
            [rss initializeServiceFromDatabase];
        }
    }

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSInteger refreshRate = [defaults integerForKey:@"FeedsRefreshRate"];
    if (refreshRate <= 0) {
        refreshRate = 5;
    }

    NSInteger dateDisplayMethod = [defaults integerForKey:@"DateDisplayMethod"];
    [MGBArticleViewController setUpdateTimeDisplayMethod:(unsigned char)dateDisplayMethod];
    [MGBArticleViewController setArticleViewIdentifier:[defaults boolForKey:@"ShowFeedsTitle"] ? @"ArticleTableRowWithFeedViewKey" : @"ArticleTableRowViewKey"];

    NSInteger feedsVisibility = [defaults integerForKey:@"FeedsVisibility"];
    [TinyTinyRSS setFeedsVisibility:(unsigned char)feedsVisibility];

    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:refreshRate * 60.0 target:self selector:@selector(updateAll:) userInfo:nil repeats:YES];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)addServiceSheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void*)contextInfo
{
    [sheet orderOut:self];

    if (self.addServiceWindowController.createdService) {
        [self.services addObject:self.addServiceWindowController.createdService];

        [self newServiceAdded:self.addServiceWindowController.createdService];
    }

    self.addServiceWindowController = nil;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)loadServicesFromDB
{
    FMDatabase *db = [[FMDatabase alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    if ([db open]) {
        self.services = [MicroRSSServiceTable fetchAllForDatabase:db withDelegate:self];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)updateAll:(id)sender
{
    self.progressBarHeight.constant = 30;
    [self.progressBar startAnimation:self];

    for (TinyTinyRSS *rss in self.services) {
        [rss performDataUpdate];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)expandAllFeeds:(id)sender
{
    [self.feedsList expandItem:nil expandChildren:YES];
    [self.feedsList reloadData];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)collapseAllFeeds:(id)sender
{
    [self.feedsList collapseItem:nil collapseChildren:YES];

    for (NSInteger i = 0; i < self.services.count; i++) {
        [self.feedsList expandItem:[self.feedsList itemAtRow:i] expandChildren:NO];
    }

    [self.feedsList reloadData];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)newServiceAdded:(TinyTinyRSS *)service
{
    [self.feedsList reloadData];

    service.delegate = self;
    self.progressBarHeight.constant = 30;
    [self.progressBar startAnimation:self];

    [service performDataUpdate];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)serviceRemoved
{
    [self.feedsList reloadData];

    self.articleView.categoryItem = nil;

    [self updateDockBadge];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)displayPreferences:(id)sender
{
    if (self.preferencesWindowController == nil) {
        MGBGeneralPrefViewController *generalPrefViewController = [[MGBGeneralPrefViewController alloc] initWithNibName:@"MGBGeneralPrefView" bundle:[NSBundle mainBundle]];
        MGBAccountsPrefViewController *accountsPrefViewController = [[MGBAccountsPrefViewController alloc] initWithNibName:@"MGBAccountsPrefView" bundle:[NSBundle mainBundle]];
        accountsPrefViewController.servicesArray = self.services;

        generalPrefViewController.delegate = self;

        MASPreferencesWindowController *prefWindow = [[MASPreferencesWindowController alloc]
                initWithViewControllers:@[generalPrefViewController, accountsPrefViewController]
                                  title:NSLocalizedString(@"Preferences", @"Preferencje")];

        self.preferencesWindowController = prefWindow;
    }

    [self.preferencesWindowController showWindow:self];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)generalPreferencesCommited:(NSDictionary *)preferences
{
    NSNumber *refreshRate      = [preferences objectForKey:@"FeedsRefreshRate"];
    NSTimeInterval newInterval = [refreshRate integerValue] * 60.0;

    if (self.updateTimer.timeInterval != newInterval) {
        [self.updateTimer invalidate];
        self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:newInterval target:self selector:@selector(updateAll:) userInfo:nil repeats:YES];
    }

    NSNumber *acceptSelfSignedCert = [preferences objectForKey:@"AcceptSelfSignedCert"];
    [TinyTinyRSS setAcceptSelfSignedCert:acceptSelfSignedCert.boolValue];

    NSNumber *dateDisplayMethodNmber = [preferences objectForKey:@"DateDisplayMethod"];
    unsigned char dateDisplayMethod  = [dateDisplayMethodNmber unsignedCharValue];

    BOOL reloadArticlesList = NO;

    if (dateDisplayMethod != [MGBArticleViewController updateTimeDisplayMethod]) {
        [MGBArticleViewController setUpdateTimeDisplayMethod:dateDisplayMethod];

        reloadArticlesList = YES;
    }

    NSString *articleRowViewID = [[preferences objectForKey:@"ShowFeedsTitle"] boolValue] ? @"ArticleTableRowWithFeedViewKey" : @"ArticleTableRowViewKey";
    if (![[MGBArticleViewController articleViewIdentifier] isEqualToString:articleRowViewID]) {
        [MGBArticleViewController setArticleViewIdentifier:articleRowViewID];

        reloadArticlesList = YES;
    }

    if (reloadArticlesList) {
        [self.articleView reloadArticlesList];
    }

    NSNumber *feedsVisibilityNumber = [preferences objectForKey:@"FeedsVisibility"];
    unsigned char feedsVisibility   = [feedsVisibilityNumber unsignedCharValue];

    if (feedsVisibility != [TinyTinyRSS feedsVisibility]) {
        [TinyTinyRSS setFeedsVisibility:feedsVisibility];
        [self updateAll:self];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setupSplitView
{
    CNSplitViewToolbarButton *refreshButton = [[CNSplitViewToolbarButton alloc] init];
    refreshButton.imageTemplate             = CNSplitViewToolbarButtonImageTemplateRefresh;
    refreshButton.title                     = NSLocalizedString(@"Refresh", @"Refresh");
    refreshButton.target                    = self;
    refreshButton.action                    = @selector(updateAll:);

    CNSplitViewToolbarButton *warningButton = [[CNSplitViewToolbarButton alloc] init];
    warningButton.image                     = [NSImage imageNamed:@"warning"];
    warningButton.target                    = self;
    warningButton.action                    = @selector(showErrorsPanel:);

    [warningButton setHidden:YES];

    CNSplitViewToolbar *toolbar = [[CNSplitViewToolbar alloc] init];
    [toolbar addItem:refreshButton align:CNSplitViewToolbarItemAlignRight];
    [toolbar addItem:warningButton align:CNSplitViewToolbarItemAlignLeft];

    self.warningButton = warningButton;

    [self.mainSplitView attachToolbar:toolbar toSubViewAtIndex:0 onEdge:CNSplitViewToolbarEdgeBottom];
    [self.mainSplitView showToolbarAnimated:NO];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setupArticleView
{
    MGBArticleViewController *articleViewController;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults integerForKey:@"AppLayout"] == 1) {
        articleViewController = [[MGBArticleViewController alloc] initWithNibName:@"MGBArticleViewWidescreen" bundle:nil];
    } else {
        articleViewController = [[MGBArticleViewController alloc] initWithNibName:@"MGBArticleView" bundle:nil];
    }

    articleViewController.delegate = self;

    NSView *articleView = articleViewController.view;

    [articleView setTranslatesAutoresizingMaskIntoConstraints:NO];

    [self.contentViewContainer addSubview:articleView];

    [[self class] addEdgeConstraint:NSLayoutAttributeLeft superview:self.contentViewContainer subview:articleView];
    [[self class] addEdgeConstraint:NSLayoutAttributeRight superview:self.contentViewContainer subview:articleView];
    [[self class] addEdgeConstraint:NSLayoutAttributeTop superview:self.contentViewContainer subview:articleView];
    [[self class] addEdgeConstraint:NSLayoutAttributeBottom superview:self.contentViewContainer subview:articleView];

    self.articleView = articleViewController;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)updateDockBadge
{
    NSInteger totalUnread = 0;

    for (TinyTinyRSS *rss in self.services) {
        totalUnread += rss.allFeed.unread;
    }


    NSDockTile *aTitle = [[NSApplication sharedApplication] dockTile];

    if (totalUnread > 0) {
        NSString *badgeValue = [[NSString alloc] initWithFormat:@"%ld", totalUnread];
        [aTitle setBadgeLabel:badgeValue];
    } else {
        [aTitle setBadgeLabel:@""];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (TinyTinyRSS *service in self.services) {
            [service syncFeedsWithDatabase];
        }

        [[NSApplication sharedApplication] replyToApplicationShouldTerminate:YES];
    });

    return NSTerminateLater;
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)showErrorsPanel:(id)sender
{
    if (self.errorWindowController) {
        [self.errorWindowController showWindow:self];
    } else {
        MGBErrorWindowController *controller = [[MGBErrorWindowController alloc] initWithWindowNibName:@"MGBErrorWindow"];
        controller.delegate = self;
        self.errorWindowController = controller;

        [controller showWindow:self];
    }

    self.errorWindowController.errorsArray = self.warningsArray;
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)markAllAsRead:(id)sender
{
    NSIndexSet *selectedIndexes = [self.feedsList selectedRowIndexes];

    if([selectedIndexes count] == 1) {
        TinyTinyRSSFeed *selectedFeed = [self.feedsList itemAtRow:[selectedIndexes firstIndex]];
        TinyTinyRSS *service = selectedFeed.service;

        self.progressBarHeight.constant = 30;
        [self.progressBar startAnimation:self];
        [selectedFeed markAllAsRead:^() {
            [service performDataUpdate];
        }];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)subscribeToFeed:(id)sender
{
    if (self.subscribeWindowController) {
        return;
    }

    if (self.services.count == 0) {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:NSLocalizedString(@"No servers are configured. Add at least one account to continue.",
            @"Brak skonfigurowanych serwerów. Aby kontynuować dodaj co najmniej jedno konto.")];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"OK")];
        [alert setAlertStyle:NSWarningAlertStyle];
        [alert runModal];

        return;
    }

    NSInteger selectedServiceID  = 0;
    NSInteger selectedCategoryID = 0;

    NSIndexSet *selectedIndexes = [self.feedsList selectedRowIndexes];

    if([selectedIndexes count] == 1) {
        TinyTinyRSSFeed *selectedFeed = [self.feedsList itemAtRow:[selectedIndexes firstIndex]];

        selectedServiceID = selectedFeed.service.serviceID;

        if (selectedFeed.isCategory) {
            selectedCategoryID = selectedFeed.feedID;
        } else if (selectedFeed.categoryID) {
            selectedCategoryID = selectedFeed.categoryID;
        }
    }

    MGBSubscribeWindowController *subscribeController;

    if ([sender isKindOfClass:[NSString class]]) {
        subscribeController = [[MGBSubscribeWindowController alloc] initWithServices:self.services
                                                                   selectedServiceID:selectedServiceID
                                                                  selectedCategoryID:selectedCategoryID
                                                                             feedURL:sender];
    } else {
        subscribeController = [[MGBSubscribeWindowController alloc] initWithServices:self.services
                                                                   selectedServiceID:selectedServiceID
                                                                  selectedCategoryID:selectedCategoryID];
    }

    self.subscribeWindowController = subscribeController;

    [NSApp beginSheet:self.subscribeWindowController.window modalForWindow:self.window
        modalDelegate:self
       didEndSelector:@selector(subscribeToFeedSheetDidEnd:returnCode:contextInfo:)
          contextInfo:nil];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)getUrl:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent
{
    NSString *url = [[event paramDescriptorForKeyword:keyDirectObject] stringValue];

    [self subscribeToFeed:url];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)subscribeToFeedSheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(id)contextInfo
{
    [sheet orderOut:self];

    if (self.subscribeWindowController.subscribedService) {
        self.progressBarHeight.constant = 30;
        [self.progressBar startAnimation:self];

        [self.subscribeWindowController.subscribedService performDataUpdate];
    }

    self.subscribeWindowController = nil;
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)unsubscribeFeed:(id)sender
{
    NSIndexSet *selectedIndexes = [self.feedsList selectedRowIndexes];

    if([selectedIndexes count] == 1) {
        TinyTinyRSSFeed *selectedFeed = [self.feedsList itemAtRow:[selectedIndexes firstIndex]];

        if (selectedFeed.isCategory || selectedFeed.categoryID < 0) {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:NSLocalizedString(@"Select feed you want to unsubscribe.", @"Wybierz kanał z którego chcesz się wypisać.")];
            [alert addButtonWithTitle:NSLocalizedString(@"OK", @"OK")];
            [alert setAlertStyle:NSInformationalAlertStyle];
            [alert runModal];
            return;
        }

        NSAlert *alert = [[NSAlert alloc] init];

        [alert addButtonWithTitle:NSLocalizedString(@"Unsubscribe", @"Wypisz się")];
        [alert addButtonWithTitle:NSLocalizedString(@"Cancel", @"Anuluj")];
        [alert setMessageText:[NSString stringWithFormat:NSLocalizedString(@"Unsubscribe from %@?", @"Wypisać się z %@?"), selectedFeed.name]];
        [alert setInformativeText:NSLocalizedString(@"Are you sure to unsubscribe from this feed?", @"Czy na pewno chcesz się wypisać z tego kanału?")];
        [alert setAlertStyle:NSInformationalAlertStyle];

        [alert beginSheetModalForWindow:self.window modalDelegate:self didEndSelector:@selector(unsubscribeAlertDidEnd:returnCode:) contextInfo:nil];
    } else {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:NSLocalizedString(@"Select feed you want to unsubscribe.", @"Wybierz kanał z którego chcesz się wypisać.")];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"OK")];
        [alert setAlertStyle:NSInformationalAlertStyle];
        [alert runModal];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)unsubscribeAlertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode
{
    if (returnCode == NSAlertFirstButtonReturn) {
        NSIndexSet *selectedIndexes = [self.feedsList selectedRowIndexes];

        if([selectedIndexes count] == 1) {
            TinyTinyRSSFeed *selectedFeed = [self.feedsList itemAtRow:[selectedIndexes firstIndex]];

            self.progressBarHeight.constant = 30;
            [self.progressBar startAnimation:self];
            [selectedFeed unsubscribe:^{
                [selectedFeed.service performDataUpdate];
                self.articleView.categoryItem = nil;
            }];
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)print:(id)sender
{
    [self.articleView print:sender];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - MGBErrorWindowControllerDelegate
- (void)errorWindowWillClose
{
    self.errorWindowController = nil;
    [self.warningsArray removeAllObjects];

    [self.warningButton setHidden:YES];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - DataServiceDelegate
- (void)errorOccurred:(NSError *)error
{
    [self.warningButton setHidden:NO];

    [self.warningsArray addObject:error];

    if (self.window.isVisible) {
        NSTextField *msgLabel = [self.errorPopover.contentViewController.view viewWithTag:2];
        msgLabel.stringValue  = [error localizedDescription];

        [self.errorPopover showRelativeToRect:self.warningButton.bounds ofView:self.warningButton preferredEdge:CGRectMaxYEdge];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)dataUpdatedForService:(DataServiceTreeItem *)service
{
    [self.feedsList reloadData];

    BOOL allDataLoaded = YES;

    for (TinyTinyRSS *rss in self.services) {
        [self.feedsList expandItem:rss expandChildren:NO];

        if (!rss.dataLoaded) {
            allDataLoaded = NO;
        }
    }

    if (allDataLoaded) {
        // ukrywanie paska ładowania
        self.progressBarHeight.constant = 0;
        [self.progressBar stopAnimation:self];
        [self allDataUpdated];
    }

    [self updateDockBadge];

    [self.articleView updateArticlesIfNeeded];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)allDataUpdated
{
    for (TinyTinyRSS *rss in self.services) {
        [rss downloadNewHeadlines:^(NSArray *headlines) {
            if (headlines.count > 0) {
                if (headlines.count >= 50) {
                    NSUserNotification *notification = [[NSUserNotification alloc] init];
                    notification.title           = NSLocalizedString(@"New articles", @"Nowe artykuły");
                    notification.informativeText = NSLocalizedString(@"More than 50 new articles available.", @"Ponad 50 nowych artykułów dostępnych.");
                    notification.userInfo        = @{
                            @"serviceID" : [NSNumber numberWithInteger:rss.serviceID]
                    };

                    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
                } else {
                    NSUserNotificationCenter *notificationCenter = [NSUserNotificationCenter defaultUserNotificationCenter];

                    for (TinyTinyRSSArticle *article in headlines) {
                        NSUserNotification *notification = [[NSUserNotification alloc] init];
                        notification.title           = article.articleTitle;
                        notification.informativeText = article.excerpt;
                        notification.userInfo        = @{
                                @"serviceID" : [NSNumber numberWithInteger:rss.serviceID],
                                @"articleID" : [NSNumber numberWithInteger:article.articleID]
                        };

                        [notificationCenter deliverNotification:notification];
                    }
                }
            }
        }];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag
{
    [self.window makeKeyAndOrderFront:nil];
    [self.reopenMenuItem setHidden:YES];

    return YES;
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)reopenMainWindow:(id)sender
{
    [self.window makeKeyAndOrderFront:nil];
    [self.reopenMenuItem setHidden:YES];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)mainWindowClosed:(id)sender
{
    [self.reopenMenuItem setHidden:NO];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - MGBArticleViewControllerDelegate
- (void)feedsDataChanged:(id)sender
{
    [self.feedsList reloadData];

    [self updateDockBadge];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)feedStarSelectionChanged:(BOOL)star
{
    if (star) {
        [self.markWithStarMenuItem setTitle:NSLocalizedString(@"Remove star", @"Usuń gwiazdkę")];
    } else {
        [self.markWithStarMenuItem setTitle:NSLocalizedString(@"Mark with star", @"Oznacz gwiazdką")];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)feedPublishSelectionChanged:(BOOL)published
{
    if (published) {
        [self.publishArticleMenuItem setTitle:NSLocalizedString(@"Unpublish", @"Usuń publikację")];
    } else {
        [self.publishArticleMenuItem setTitle:NSLocalizedString(@"Publish", @"Publikuj")];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSMenu *)sourceList:(PXSourceList *)aSourceList menuForEvent:(NSEvent *)theEvent item:(id)item
{
    if ([theEvent type] == NSRightMouseDown || ([theEvent type] == NSLeftMouseDown && ([theEvent modifierFlags] & NSControlKeyMask) == NSControlKeyMask)) {
        if ([item isKindOfClass:[TinyTinyRSSFeed class]]) {
            [aSourceList selectItem:item];

            TinyTinyRSSFeed *rssFeed = (TinyTinyRSSFeed *)item;
            if (!rssFeed.isCategory && rssFeed.categoryID >= 0) {
                // tylko dla feedów nie będących kategoriami oraz feedami specjalnymi
                NSMenu *rClickMenu = [[NSMenu alloc] init];

                [rClickMenu addItemWithTitle:NSLocalizedString(@"Mark All as Read", @"Oznacz wszystko jako przeczytane") action:@selector(markAllAsRead:) keyEquivalent:@""];
                [rClickMenu addItemWithTitle:NSLocalizedString(@"Unsubscribe", @"Rezygnacja z subskrybcji") action:@selector(unsubscribeFeed:) keyEquivalent:@""];

                return rClickMenu;
            }
        }
    }
    return nil;
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - PXSourceListDataSource
- (NSUInteger)sourceList:(PXSourceList *)sourceList numberOfChildrenOfItem:(id)item
{
    return (item == nil) ? [self.services count] : [(DataServiceTreeItem *)item numberOfChildren];
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)sourceList:(PXSourceList *)aSourceList child:(NSUInteger)index1 ofItem:(id)item
{
    if (item == nil) {
        return [self.services objectAtIndex:index1];
    } else {
        DataServiceTreeItem *treeItem = (DataServiceTreeItem *)item;

        return [[treeItem children] objectAtIndex:index1];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)sourceList:(PXSourceList *)aSourceList objectValueForItem:(id)item
{
    return [(DataServiceTreeItem *)item title];
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)sourceList:(PXSourceList *)aSourceList isItemExpandable:(id)item
{
    return [(DataServiceTreeItem *)item isExpandable];
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)sourceList:(PXSourceList *)aSourceList itemHasBadge:(id)item
{
    return [(DataServiceTreeItem *)item hasBadge];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSInteger)sourceList:(PXSourceList *)aSourceList badgeValueForItem:(id)item
{
    return [(DataServiceTreeItem *)item badgeValue];
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)sourceList:(PXSourceList *)aSourceList itemHasIcon:(id)item
{
    return [(DataServiceTreeItem *)item hasIcon];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSImage *)sourceList:(PXSourceList *)aSourceList iconForItem:(id)item
{
    return [(DataServiceTreeItem *)item icon];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark PXSourceListDelegate
- (BOOL)sourceList:(PXSourceList *)aSourceList isGroupAlwaysExpanded:(id)group
{
    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)sourceList:(PXSourceList *)aSourceList shouldEditItem:(id)item
{
    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (CGFloat)sourceList:(PXSourceList *)aSourceList heightOfRowByItem:(id)item
{
    return 24;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)sourceListSelectionDidChange:(NSNotification *)notification
{
    NSIndexSet *selectedIndexes = [self.feedsList selectedRowIndexes];

    if([selectedIndexes count] == 1) {
        DataServiceTreeItem *selectedFeed = [self.feedsList itemAtRow:[selectedIndexes firstIndex]];

        self.articleView.categoryItem = selectedFeed;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark NSUserNotificationCenterDelegate
- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification
{
    return YES;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification
{
    NSInteger serviceID = [(NSNumber *)[notification.userInfo objectForKey:@"serviceID"] integerValue];

    // Wyszukiwanie serwisu do zaznaczenia
    TinyTinyRSS *selectedService;
    for (TinyTinyRSS *rss in self.services) {
        if (rss.serviceID == serviceID) {
            selectedService = rss;
            break;
        }
    }

    if (!selectedService) return;

    NSNumber *articleNumber = [notification.userInfo objectForKey:@"articleID"];
    if (articleNumber) {
        self.articleView.articleToSelect = [articleNumber integerValue];
    }

    [self.feedsList selectItem:selectedService.allFeed];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark Inne
+ (void)addEdgeConstraint:(NSLayoutAttribute)edge superview:(NSView *)superview subview:(NSView *)subview
{
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                          attribute:edge
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:edge
                                                         multiplier:1
                                                           constant:0]];
}

@end