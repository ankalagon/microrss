//
// Created by Grzesiek on 27.05.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "DataServiceTreeItem.h"

@interface TinyTinyRSSCategory : DataServiceTreeItem

@property (nonatomic, retain) NSNumber *categoryID;

@property (nonatomic, retain) NSString *name;

@property (nonatomic, retain) NSNumber *unread;

@property(nonatomic, retain) NSMutableArray *feeds;

/**
* Inicjalizuje obiekt na podstawie słownika odczytanego przez przetważanie odpowiedzi JSON z serwera
*
* @param dictionary słownik użyty do inicjalizacji
*/
- (id)initWithDictionary:(NSDictionary *)dictionary;
@end