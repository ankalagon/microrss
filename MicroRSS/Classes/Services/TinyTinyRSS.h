//
// Created by Grzesiek on 26.05.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "DataServiceTreeItem.h"

#define MGB_FEEDS_VISIBILITY_ALL 0
#define MGB_FEEDS_VISIBILITY_UNREAD 1

@protocol DataServiceDelegate;
@class TinyTinyRSSFeed;

/**
* Klasa pobierająca dane z TinyTinyRSS
*/
@interface TinyTinyRSS : DataServiceTreeItem

/**
* Numer ID serwisu w bazie danych
*/
@property (nonatomic, assign) NSInteger serviceID;

/**
* Nazwa serwisu z danymi
*/
@property (nonatomic, retain) NSString *name;

/**
* Lista feedów
*/
@property (nonatomic, retain) NSArray *feeds;

/**
* Indeks do szybkiego wyszukiwania feed'ów
*/
@property (nonatomic, retain) NSMutableDictionary *feedsIndex;

/**
* Indeks do szybkiego wyszukiwania kategorii
*/
@property (nonatomic, retain) NSMutableDictionary *categoryIndex;

/**
* Feed z listą wszystkich artykułów
*/
@property (nonatomic, retain) TinyTinyRSSFeed *allFeed;

/**
* Feed z listą świeżych artykułow
*/
@property (nonatomic, retain) TinyTinyRSSFeed *freshFeed;

/**
* Feed z listą artykułow oznaczonych gwiazdką
*/
@property (nonatomic, retain) TinyTinyRSSFeed *starredFeed;

/**
* Feed z listą opublikowanych artykułow
*/
@property (nonatomic, retain) TinyTinyRSSFeed *publishedFeed;

/**
* Adres URL do API.
*/
@property (nonatomic, retain) NSURL *apiURL;

/**
* Delegat do którego zgłaszane będą zdarzenia aktualizacji danych.
*/
@property (nonatomic, assign) id<DataServiceDelegate> delegate;

/**
* Login do aplikacji TTRSS
*/
@property (nonatomic, retain) NSString *login;

/**
* Hasło do aplikacji TTRSS
*/
@property (nonatomic, retain) NSString *password;

/**
* Ustawiane na YES po zakończeniu operacji pobierania danych
*/
@property (nonatomic, assign) BOOL dataLoaded;

/**
* Maksymalny czas "życia" świeżych artykułów
*/
@property (nonatomic, assign, readonly) NSInteger freshArticleMaxAge;

/**
* Inicjalizuje nową instancję klasy.
*/
- (id)initWithApiURL:(NSURL *)apiURL delegate:(id <DataServiceDelegate>)delegate login:(NSString *)login password:(NSString *)password name:(NSString *)name;

/**
* Ustawia sposób wyświetlania feedów
*/
+ (void)setFeedsVisibility:(unsigned char)visibility;

/**
* Zwraca sposób wyświetlania feedów
*/
+ (unsigned char)feedsVisibility;

/**
* Ustawia akceptację samo podpisanych certyfikatów
*/
+ (void)setAcceptSelfSignedCert:(BOOL)accept1;

/**
* Zwraca aktualną wartość akceptacji samo podpisanych certyfikatów
*/
+ (BOOL)acceptSelfSignedCert;

/**
* Wczytuje dane z bazy a następnie uruchamia aktualizację z serwera
*/
- (void)initializeServiceFromDatabase;

/**
* Wykonuje pełną aktualizację danych serwisu.
*/
- (void)performDataUpdate;

/**
* Komunikat błędu: nie udało się zalogować
*/
- (void)errorNotLoggedIn;

/**
* Komunikat błędu zwrócony z TTRSS
*/
- (void)errorTTRSS:(NSString *)errorMessage;

/**
* Komunikat błędu połączenia
*/
- (void)errorConnection:(NSError *)connectionError;

/**
* Przeprowadza test połączenia wykonując próbę logowania. Operacja wykonywana jest w tle. Gdy zakończy się odpalana
* jest funkcja callback.
*/
- (void)testConnection:(void (^)(BOOL))callback;

/**
* Przeprowadza operację logowania
*/
- (BOOL)performLogin;

/**
* Wylogowuje z konta
*/
- (void)performLogout;

/**
* Synchronizuje feedy z bazą danych
*/
- (void)syncFeedsWithDatabase;

/**
* Zwraca ścieżkę w której przechowywane są ikony
*/
- (NSURL *)iconsPath;

/**
* Przeprowadza operację logowania
*/
- (void)subscribeToFeed:(NSString *)feedURL forCategory:(NSInteger)categoryID callback:(void (^)(NSError *error))callback;

/**
* Pobiera nowe nagłówki
*/
- (void)downloadNewHeadlines:(void (^)(NSArray *))callback;

/**
* Zwraca numer ID sesji
*/
- (NSString *)sessionIdentifier;

/**
* Zapisuje zmiany do bazy
*/
- (void)commit;

/**
* Usuwa serwis z bazy
*/
- (void)removeFromDB;
@end