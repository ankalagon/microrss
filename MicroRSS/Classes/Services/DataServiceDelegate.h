//
// Created by Grzesiek on 26.05.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@class DataServiceTreeItem;

@protocol DataServiceDelegate <NSObject>

/**
* Metoda wywoływana po wystąpieniu błędu
*/
- (void)errorOccurred:(NSError *)error;

@required
/**
* Wywoływana po zakończeniu aktualizacji serwisu
*/
- (void)dataUpdatedForService:(DataServiceTreeItem *)service;

@end