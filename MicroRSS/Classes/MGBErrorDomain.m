//
// Created by Grzesiek on 03.08.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "MGBErrorDomain.h"


@implementation MGBErrorDomain

// ---------------------------------------------------------------------------------------------------------------------
+ (NSString *)domain
{
    return @"eu.mobilegb.MGBErrorDomain";
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSError *)errorWithErrorCode:(NSInteger)errorCode
{
    NSError *error = [NSError errorWithDomain:[self domain] code:errorCode userInfo:@{}];

    return error;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSError *)errorWithErrorCode:(NSInteger)errorCode userInfo:(NSDictionary *)userInfo
{
    NSError *error = [NSError errorWithDomain:[self domain] code:errorCode userInfo:userInfo];

    return error;
}


// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)domain
{
    return [[self class] domain];
}

// ---------------------------------------------------------------------------------------------------------------------

@end