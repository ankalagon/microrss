//
// Created by Grzesiek on 03.08.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol MGBErrorWindowControllerDelegate <NSObject>

/**
* Zdarzenie wysyłane przed zamknięciem okna.
*/
- (void)errorWindowWillClose;

@end