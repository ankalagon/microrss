//
//  MGBAddServiceWindowController.m
//  MicroRSS
//
//  Created by Grzesiek on 07.09.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import "MGBAddServiceWindowController.h"
#import "TinyTinyRSS.h"
#import "MGBCustomBackgroundView.h"

@interface MGBAddServiceWindowController ()

@end

@implementation MGBAddServiceWindowController

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)windowDidLoad
{
    [super windowDidLoad];
    
    self.errorInfoView.backgroundRenderer = ^(NSRect dirtyRect, MGBCustomBackgroundView *view) {
        //// Color Declarations
        NSColor* borderColor = [NSColor colorWithCalibratedRed: 0.713 green: 0 blue: 0 alpha: 1];
        NSColor* backgroundColor = [NSColor colorWithCalibratedRed: 0.993 green: 0.783 blue: 0.783 alpha: 1];

        //// Rectangle Drawing
        NSBezierPath* rectanglePath = [NSBezierPath bezierPathWithRect: view.bounds];
        [backgroundColor setFill];
        [rectanglePath fill];
        [borderColor setStroke];
        [rectanglePath setLineWidth: 1];
        [rectanglePath stroke];
    };
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)addAccount:(id)sender
{
    [self.errorInfoView setHidden:YES];

    if (self.nameField.stringValue.length == 0) {
        self.errorLabel.stringValue = NSLocalizedString(@"Please enter service name", @"Komunikat wyświetlany gdy nie wprowadzono nazy serwisu");
        [self.errorInfoView setHidden:NO];
        return;
    }

    if (self.loginField.stringValue.length == 0) {
        self.errorLabel.stringValue = NSLocalizedString(@"Please enter login", @"Komunikat wyświetlany gdy nie wprowadzono loginu");
        [self.errorInfoView setHidden:NO];
        return;
    }

    if (self.passwordField.stringValue.length == 0) {
        self.errorLabel.stringValue = NSLocalizedString(@"Please enter password", @"Komunikat wyświetlany gdy nie wprowadzono hasła");
        [self.errorInfoView setHidden:NO];
        return;
    }

    if (![self.passwordField.stringValue isEqualToString:self.repeatPasswordField.stringValue]) {
        self.errorLabel.stringValue = NSLocalizedString(@"Fields password and confirm password do not match", @"Komunikat wyświetlany gdy wprowadzone hasła różnią się");
        [self.errorInfoView setHidden:NO];
        return;
    }

    NSMutableString *apiUrl = [[NSMutableString alloc] initWithString:self.urlField.stringValue];

    if (apiUrl.length <= 8) {
        self.errorLabel.stringValue = NSLocalizedString(@"Please enter valid URL address", @"Komunikat wyświetlany wprowadzono nieprawidłowy adres URL");
        [self.errorInfoView setHidden:NO];
        return;
    }

    TinyTinyRSS *newService = [[TinyTinyRSS alloc] init];

    newService.name     = self.nameField.stringValue;
    newService.login    = self.loginField.stringValue;
    newService.password = self.passwordField.stringValue;

    // dodawanie do adresu URL /
    if ([apiUrl characterAtIndex:apiUrl.length -1] != '/') {
        [apiUrl appendString:@"/"];
    }

    // sprwdzanie czy adres URL kończy się na /api/
    if (![[apiUrl substringFromIndex:apiUrl.length -5] isEqualToString:@"/api/"]) {
        [apiUrl appendString:@"api/"];
    }

    // upewnianie się że adres URL rozpoczyna się od http lub https
    if (![[apiUrl substringToIndex:7] isEqualToString:@"http://"] && ![[apiUrl substringToIndex:8] isEqualToString:@"https://"]) {
        newService.apiURL = [[NSURL alloc] initWithString:[@"http://" stringByAppendingString:apiUrl]];
    } else {
        newService.apiURL = [[NSURL alloc] initWithString:apiUrl];
    }

    [self.waitSpinner startAnimation:self];
    [newService testConnection:^(BOOL ok) {
        [self.waitSpinner stopAnimation:self];
        if (ok) {
            [newService commit];
            self.createdService = newService;

            [NSApp endSheet:self.window];
        } else {
            self.errorLabel.stringValue = NSLocalizedString(@"Unable to connect using provided settings", @"Komunikat o niemożności połączenia się za pomocą podanych danych");
            [self.errorInfoView setHidden:NO];
        }
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)cancelAddOperation:(id)sender
{
    [NSApp endSheet:self.window];
}


@end
