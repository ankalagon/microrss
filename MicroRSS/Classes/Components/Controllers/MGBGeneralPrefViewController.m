//
//  MGBGeneralPrefViewController.m
//  MicroRSS
//
//  Created by Grzesiek on 31.08.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import "MGBGeneralPrefViewController.h"

@interface MGBGeneralPrefViewController ()

@end

@implementation MGBGeneralPrefViewController

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)identifier
{
    return @"General";
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSImage *)toolbarItemImage
{
    return [NSImage imageNamed:NSImageNamePreferencesGeneral];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)toolbarItemLabel
{
    return NSLocalizedString(@"General", @"Etykieta dla preferencji ogólnych");
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)viewWillAppear
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSInteger refreshRate = [defaults integerForKey:@"FeedsRefreshRate"];

    if (refreshRate <= 0) {
        refreshRate = 5;
    }

    [self.refreshRateField setIntegerValue:refreshRate];

    NSInteger sortingOrder = [defaults integerForKey:@"FeedsSortingOrder"];

    [self.sortingOrderSelect selectItemAtIndex:sortingOrder];

    NSInteger dateDisplayMethod = [defaults integerForKey:@"DateDisplayMethod"];

    [self.articleDateDisplayMethodSelect selectItemAtIndex:dateDisplayMethod];

    NSInteger feedsVisibility = [defaults integerForKey:@"FeedsVisibility"];

    [self.feedsVisibilitySelect selectItemAtIndex:feedsVisibility];

    [self.showFeedsTitleCheckbox setState:[defaults boolForKey:@"ShowFeedsTitle"] ? NSOnState : NSOffState];

    NSInteger applicationLayout = [defaults integerForKey:@"AppLayout"];

    [self.layoutSelect selectItemAtIndex:applicationLayout];

    [self.acceptSelfSignedCertCheckbox setState:[defaults boolForKey:@"AcceptSelfSignedCert"] ? NSOnState : NSOffState];
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)commitEditing
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSInteger refreshRate = [self.refreshRateField integerValue];

    if (refreshRate <= 0) {
        refreshRate = 5;
    }

    [defaults setInteger:refreshRate forKey:@"FeedsRefreshRate"];

    NSInteger sortingOrder = [self.sortingOrderSelect indexOfSelectedItem];
    [defaults setInteger:sortingOrder forKey:@"FeedsSortingOrder"];

    NSInteger dateDisplayMethod = [self.articleDateDisplayMethodSelect indexOfSelectedItem];
    [defaults setInteger:dateDisplayMethod forKey:@"DateDisplayMethod"];

    NSInteger feedsVisibility = [self.feedsVisibilitySelect indexOfSelectedItem];
    [defaults setInteger:feedsVisibility forKey:@"FeedsVisibility"];

    BOOL showFeedsTitle = [self.showFeedsTitleCheckbox state] == NSOnState;
    [defaults setBool:showFeedsTitle forKey:@"ShowFeedsTitle"];

    NSInteger selectedLayout = [self.layoutSelect indexOfSelectedItem];
    [defaults setInteger:selectedLayout forKey:@"AppLayout"];

    BOOL acceptSelfSignedCert = [self.acceptSelfSignedCertCheckbox state] == NSOnState;
    [defaults setBool:acceptSelfSignedCert forKey:@"AcceptSelfSignedCert"];

    [defaults synchronize];

    [self.delegate generalPreferencesCommited:@{
            @"FeedsRefreshRate"     : [NSNumber numberWithInteger:refreshRate],
            @"FeedsSortingOrder"    : [NSNumber numberWithInteger:sortingOrder],
            @"DateDisplayMethod"    : [NSNumber numberWithInteger:dateDisplayMethod],
            @"FeedsVisibility"      : [NSNumber numberWithInteger:feedsVisibility],
            @"ShowFeedsTitle"       : [NSNumber numberWithBool:showFeedsTitle],
            @"AppLayout"            : [NSNumber numberWithInteger:selectedLayout],
            @"AcceptSelfSignedCert" : [NSNumber numberWithBool:acceptSelfSignedCert]
    }];

    return [super commitEditing];
}


@end
