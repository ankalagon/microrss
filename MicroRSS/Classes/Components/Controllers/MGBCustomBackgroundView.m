//
//  MGBCustomBackgroundView.m
//  MicroRSS
//
//  Created by Grzesiek on 07.09.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import "MGBCustomBackgroundView.h"

@implementation MGBCustomBackgroundView

// ---------------------------------------------------------------------------------------------------------------------
- (void)drawRect:(NSRect)dirtyRect
{
    if (self.backgroundRenderer) {
        self.backgroundRenderer(dirtyRect, self);
    } else {
        [super drawRect:dirtyRect];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setBackgroundRenderer:(MGBCustomBackgroundViewRenderer)backgroundRenderer
{
    _backgroundRenderer = backgroundRenderer;

    [self setNeedsDisplay:YES];
}


@end
