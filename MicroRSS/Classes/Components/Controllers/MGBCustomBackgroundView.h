//
//  MGBCustomBackgroundView.h
//  MicroRSS
//
//  Created by Grzesiek on 07.09.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MGBCustomBackgroundView;

typedef void (^MGBCustomBackgroundViewRenderer)(NSRect, MGBCustomBackgroundView *);

@interface MGBCustomBackgroundView : NSView

/**
* Funkcja renderująca tło.
*/
@property (nonatomic, assign) MGBCustomBackgroundViewRenderer backgroundRenderer;

@end