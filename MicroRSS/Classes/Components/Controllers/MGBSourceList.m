//
// Created by Grzesiek on 12.09.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "MGBSourceList.h"
#import "MGBAppDelegate.h"


@implementation MGBSourceList

// ---------------------------------------------------------------------------------------------------------------------
- (void)print:(id)sender
{
    MGBAppDelegate *appDelegate = (MGBAppDelegate *)[[NSApplication sharedApplication] delegate];

    [appDelegate print:sender];
}

@end