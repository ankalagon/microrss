//
//  MGBArticlePreviewWindowController.m
//  MicroRSS
//
//  Created by Grzesiek on 26.01.2014.
//  Copyright (c) 2014 mobilegb.eu. All rights reserved.
//

#import "MGBArticlePreviewWindowController.h"
#import "GRGlassWindow.h"
#import "AFHTTPRequestOperationManager.h"
#import "JXReadabilityDocument.h"
#import "TinyTinyRSS.h"
#import <WebKit/WebView.h>
#import <WebKit/WebKit.h>

@interface MGBArticlePreviewWindowController ()

// Dodaje do wszystkich linków target="_blank"
- (void)addHrefTarget:(NSXMLNode *)element;

// Wstawia CSS do podglądu
- (void)insertCSS:(NSXMLDocument *)document;

// Sprawdzanie czy pobrana treść nie jest pusta
- (void)checkIfContentNotEmpty:(NSXMLDocument *)document forUrl:(NSString *)url;

// Wczytuje do podglądu wskaźnik wczytywania
- (void)loadSpinner;
@end

@implementation MGBArticlePreviewWindowController

// ---------------------------------------------------------------------------------------------------------------------
- (void)windowDidLoad
{
    [super windowDidLoad];
    
    GRGlassWindow *window = (GRGlassWindow *)self.window;
    window.hidesTitleBar  = YES;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)loadPreviewForURL:(NSString *)url
{
    [self loadSpinner];

    [[AFSecurityPolicy defaultPolicy] setAllowInvalidCertificates:[TinyTinyRSS acceptSelfSignedCert]];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSStringEncoding encoding = NSUTF8StringEncoding;
        NSString *contentType     = operation.response.allHeaderFields[@"Content-Type"];

        if (contentType) {
            NSRange charsetPos = [contentType rangeOfString:@"charset="];
            if (charsetPos.location != NSNotFound) {
                NSString *stringEncoding = [contentType substringFromIndex:charsetPos.location + 8];

                CFStringEncoding aEncoding = CFStringConvertIANACharSetNameToEncoding((__bridge CFStringRef)stringEncoding);
                encoding = CFStringConvertEncodingToNSStringEncoding(aEncoding);
            }
        }

        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:encoding];
        if (!responseString) {
            responseString = [[NSString alloc] initWithData:responseObject encoding:[NSString defaultCStringEncoding]];
        }

        NSError *error = nil;
        NSXMLDocument *doc = [[NSXMLDocument alloc] initWithXMLString:responseString
                                                              options:NSXMLDocumentTidyHTML
                                                                error:&error];

        JXReadabilityDocument *readabilityDoc = [[JXReadabilityDocument alloc] initWithXMLDocument:doc copyDocument:NO];
        NSXMLDocument *summaryDoc             = [readabilityDoc summaryXMLDocument];

        [self addHrefTarget:summaryDoc.rootElement];
        [self insertCSS:summaryDoc];
        [self checkIfContentNotEmpty:summaryDoc forUrl:url];

        [self.webView.mainFrame loadHTMLString:[summaryDoc XMLString] baseURL:[[NSURL alloc] initWithString:url]];

        GRGlassWindow *window = (GRGlassWindow *)self.window;
        window.title          = readabilityDoc.title;
        window.subtitle       = url;

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)addHrefTarget:(NSXMLNode *)element
{
    for (NSXMLNode *node in element.children) {
        if ([node.name isEqualToString:@"a"] && [node class] == [NSXMLElement class]) {
            NSXMLNode *targetBlank = [NSXMLNode attributeWithName:@"target" stringValue:@"_blank"];
            [(NSXMLElement *)node addAttribute:targetBlank];
        } else {
            if (node.children.count > 0) {
                [self addHrefTarget:node];
            }
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)insertCSS:(NSXMLDocument *)document
{
    NSArray *headArray = [document.rootElement elementsForName:@"head"];

    if (headArray.count > 0) {
        NSXMLElement *head = headArray[0];

        NSString *cssPath    = [[NSBundle mainBundle] pathForResource:@"ArticlePreview" ofType:@"css"];
        NSString *cssContent = [NSString stringWithContentsOfFile:cssPath encoding:NSUTF8StringEncoding error:nil];

        NSXMLElement *cssElement = [[NSXMLElement alloc] initWithName:@"style" stringValue:cssContent];
        NSXMLNode *cssAttribute  = [NSXMLNode attributeWithName:@"type" stringValue:@"text/css"];

        [cssElement addAttribute:cssAttribute];

        [head addChild:cssElement];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)checkIfContentNotEmpty:(NSXMLDocument *)document forUrl:(NSString *)url
{
    NSArray *contentArray = [document nodesForXPath:@"//div[@id='article_body']" error:nil];
    NSXMLElement *element = nil;

    if (contentArray.count > 0) {
        element = (NSXMLElement *)contentArray[0];
    } else {
        contentArray = [document nodesForXPath:@"//body" error:nil];

        if (contentArray.count > 0) {
            element = (NSXMLElement *)contentArray[0];
        }
    }

    if (element) {
        if ([element.stringValue isEqualToString:@""]) {
            NSString *info = NSLocalizedString(@"Unable to detect article content.", @"Treść powiadomienia o braku możliwości załadowania treści strony.");

            NSXMLElement *infoElement = [[NSXMLElement alloc] initWithXMLString:[NSString stringWithFormat:@"<div style=\"padding-top: 60px; padding-bottom: 60px; text-align: center;\">%@</div>", info] error:nil];

            [element addChild:infoElement];

            NSXMLElement *linkElement = [[NSXMLElement alloc]
                    initWithXMLString:[NSString stringWithFormat:@"<div style=\"text-align: center;\"><a href=\"%@\">%@</a></div>",
                                    url,
                                    NSLocalizedString(@"Open in web browser.", @"Tekst linku otwierającego artykół w przeglądarce internetowej")] error:nil];

            [element addChild:linkElement];
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)loadSpinner
{
    NSString *htmlPath    = [[NSBundle mainBundle] pathForResource:@"Spinner" ofType:@"html"];
    NSString *htmlContent = [NSString stringWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];

    [self.webView.mainFrame loadHTMLString:htmlContent baseURL:nil];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - WebPolicyDelegate
- (void)webView:(WebView *)webView decidePolicyForNavigationAction:(NSDictionary *)actionInformation request:(NSURLRequest *)request frame:(WebFrame *)frame
        decisionListener:(id < WebPolicyDecisionListener >)listener
{
    if ([actionInformation objectForKey:@"WebActionElementKey"]) {
        [[NSWorkspace sharedWorkspace] openURL:[request URL]];
    } else {
        [listener use];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)webView:(WebView *)webView decidePolicyForNewWindowAction:(NSDictionary *)actionInformation request:(NSURLRequest *)request
   newFrameName:(NSString *)frameName decisionListener:(id < WebPolicyDecisionListener >)listener
{
    [[NSWorkspace sharedWorkspace] openURL:[request URL]];
}

// ---------------------------------------------------------------------------------------------------------------------

@end
