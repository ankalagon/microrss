//
// Created by Grzesiek on 25.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol ArticleTableRowViewDelegate;


@interface ArticleTableRowView : NSTableRowView

@property (nonatomic, assign) IBOutlet NSTextField *titleLabel;

@property (nonatomic, assign) IBOutlet NSTextField *excerptLabel;

@property (nonatomic, assign) IBOutlet NSButton *starButton;

@property (nonatomic, assign) IBOutlet NSButton *publishButton;

@property (nonatomic, assign) IBOutlet NSTextField *elapsedTimeLabel;

@property (nonatomic, assign) IBOutlet NSTextField *feedTitleLabel;

@property (nonatomic, assign) IBOutlet NSImageView *iconView;

@property (nonatomic, assign) BOOL unread;

@property (nonatomic, assign) NSInteger tableRow;

@property (nonatomic, assign) id<ArticleTableRowViewDelegate> delegate;

/**
* Kliknięcie guzika gwiazdki
*/
- (IBAction)starButtonClicked:(id)sender;

/**
* Kliknięcie guzika publikacji
*/
- (IBAction)publishButtonClicked:(id)sender;

/**
* Kliknięcie guzika podglądu
*/
- (IBAction)previewButtinClicked:(id)sender;
@end