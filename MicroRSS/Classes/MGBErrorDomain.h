//
// Created by Grzesiek on 03.08.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface MGBErrorDomain : NSObject

+ (NSString *)domain; // << required override
- (NSString *)domain; // << returns [[self class] domain]

// example convenience methods:
// uses [self domain]
+ (NSError *)errorWithErrorCode:(NSInteger)errorCode; // << user info would be nil
+ (NSError *)errorWithErrorCode:(NSInteger)errorCode userInfo:(NSDictionary *)userInfo;

@end