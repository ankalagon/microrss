//
// Created by Grzesiek on 14.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <FMDB/FMDatabase.h>
#import "MicroRSSDatabase.h"


@implementation MicroRSSDatabase

// ---------------------------------------------------------------------------------------------------------------------
+ (NSURL *)databasePath
{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* bundleID         = [[NSBundle mainBundle] bundleIdentifier];
    NSArray* urlPaths          = [fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];

    NSURL* appDirectory = [[urlPaths objectAtIndex:0] URLByAppendingPathComponent:bundleID isDirectory:YES];

    if (![fileManager fileExistsAtPath:[appDirectory path]]) {
        [fileManager createDirectoryAtURL:appDirectory withIntermediateDirectories:NO attributes:nil error:nil];
    }

    return [appDirectory URLByAppendingPathComponent:@"db.sqlite"];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSString *)databasePathString
{
    return [[self databasePath] absoluteString];
}

// ---------------------------------------------------------------------------------------------------------------------

@end