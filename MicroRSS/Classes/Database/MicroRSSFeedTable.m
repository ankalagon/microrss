//
// Created by Grzesiek on 05.07.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <FMDB/FMDatabaseQueue.h>
#import <FMDB/FMDatabase.h>
#import "MicroRSSFeedTable.h"
#import "TinyTinyRSSFeed.h"
#import "MicroRSSDatabase.h"
#import "TinyTinyRSS.h"


@implementation MicroRSSFeedTable

// ---------------------------------------------------------------------------------------------------------------------
+ (void)updateTTRSSFeed:(TinyTinyRSSFeed *)feed
{
    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        if (feed.dbID) {
            [db executeUpdate:@"UPDATE default_feed SET category_id = ?, name = ?, unread = ?, last_update = ?, "
                                      "last_server_update = ?, has_feed_icon = ?, order_id = ?, article_sort_order = ?"
                                      "article_visibility = ? WHERE id = ?",
                              feed.categoryID ? [NSNumber numberWithInteger:feed.categoryID] : nil,
                              feed.name,
                              [NSNumber numberWithInteger:feed.unread],
                              [NSNumber numberWithInteger:feed.lastUpdate],
                              [NSNumber numberWithInteger:feed.lastServerUpdate],
                              [NSNumber numberWithBool:feed.hasFeedIcon],
                              [NSNumber numberWithInteger:feed.orderID],
                              [NSNumber numberWithInteger:feed.articlesSortOrder],
                              [NSNumber numberWithInteger:feed.articlesVisibility],
                              [NSNumber numberWithInteger:feed.dbID]
            ];
        } else {
            [db executeUpdate:@"INSERT INTO default_feed (service_id, feed_id, category_id, name, unread, last_update, "
                                      "last_server_update, has_feed_icon, is_category, order_id, article_sort_order, "
                                      "article_visibility) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                              [NSNumber numberWithInteger:feed.service.serviceID],
                              [NSNumber numberWithInteger:feed.feedID],
                              feed.categoryID ? [NSNumber numberWithInteger:feed.categoryID] : nil,
                              feed.name,
                              [NSNumber numberWithInteger:feed.unread],
                              [NSNumber numberWithInteger:feed.lastUpdate],
                              [NSNumber numberWithInteger:feed.lastServerUpdate],
                              [NSNumber numberWithBool:feed.hasFeedIcon],
                              [NSNumber numberWithBool:feed.isCategory],
                              [NSNumber numberWithInteger:feed.orderID],
                              [NSNumber numberWithInteger:feed.articlesSortOrder],
                              [NSNumber numberWithInteger:feed.articlesVisibility]
            ];

            feed.dbID = (NSInteger)[db lastInsertRowId];
        }

        if (db.hadError) {
            NSLog(@"%@", [db lastError]);
        }
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)updateTTRSSFeeds:(NSArray *)feeds
{
    FMDatabase *db = [FMDatabase databaseWithPath:[MicroRSSDatabase databasePathString]];

    if (![db open]) {
        return;
    }

    for (TinyTinyRSSFeed *feed in feeds) {
        if (feed.dbID) {
            [db executeUpdate:@"UPDATE default_feed SET category_id = ?, name = ?, unread = ?, last_update = ?, "
                                      "last_server_update = ?, has_feed_icon = ?, order_id = ?, article_sort_order = ?, "
                                      "article_visibility = ? WHERE id = ?",
                              feed.categoryID ? [NSNumber numberWithInteger:feed.categoryID] : nil,
                              feed.name,
                              [NSNumber numberWithInteger:feed.unread],
                              [NSNumber numberWithInteger:feed.lastUpdate],
                              [NSNumber numberWithInteger:feed.lastServerUpdate],
                              [NSNumber numberWithBool:feed.hasFeedIcon],
                              [NSNumber numberWithInteger:feed.orderID],
                              [NSNumber numberWithInteger:feed.articlesSortOrder],
                              [NSNumber numberWithInteger:feed.articlesVisibility],
                              [NSNumber numberWithInteger:feed.dbID]
            ];
        } else {
            [db executeUpdate:@"INSERT INTO default_feed (service_id, feed_id, category_id, name, unread, last_update, "
                                      "last_server_update, has_feed_icon, is_category, order_id, article_sort_order, "
                                      "article_visibility) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                              [NSNumber numberWithInteger:feed.service.serviceID],
                              [NSNumber numberWithInteger:feed.feedID],
                              feed.categoryID ? [NSNumber numberWithInteger:feed.categoryID] : nil,
                              feed.name,
                              [NSNumber numberWithInteger:feed.unread],
                              [NSNumber numberWithInteger:feed.lastUpdate],
                              [NSNumber numberWithInteger:feed.lastServerUpdate],
                              [NSNumber numberWithBool:feed.hasFeedIcon],
                              [NSNumber numberWithBool:feed.isCategory],
                              [NSNumber numberWithInteger:feed.orderID],
                              [NSNumber numberWithInteger:feed.articlesSortOrder],
                              [NSNumber numberWithInteger:feed.articlesVisibility]
            ];
            feed.dbID = (NSInteger)[db lastInsertRowId];
        }

        if (db.hadError) {
            NSLog(@"%@", [db lastError]);
        }
    }

    [db close];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)removeFeedFromDatabase:(NSInteger)feedDatabaseID
{
    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        NSNumber *dbID = [[NSNumber alloc] initWithInteger:feedDatabaseID];

        [db executeUpdate:@"DELETE FROM default_feed WHERE id = ?", dbID];
        [db executeUpdate:@"DELETE FROM default_article WHERE feed_db_id = ?", dbID];
        [db executeUpdate:@"DELETE FROM default_article_max_id WHERE id = ?", dbID];
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSArray *)fetchTTRSSFeedsForService:(NSInteger)serviceID
{
    FMDatabase *db = [[FMDatabase alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    if (![db open]) {
        return nil;
    }

    NSString *orderByQueryPart;
    NSInteger orderBy = [[NSUserDefaults standardUserDefaults] integerForKey:@"FeedsSortingOrder"];
    if (orderBy == 0) {
        orderByQueryPart = @"name ASC";
    } else {
        orderByQueryPart = @"order_id ASC, name ASC";
    }

    NSMutableArray *array = [[NSMutableArray alloc] init];

    NSString *query        = [[NSString alloc] initWithFormat:@"SELECT * FROM default_feed WHERE service_id = ? ORDER BY is_category DESC, %@", orderByQueryPart];
    NSNumber *servID       = [[NSNumber alloc] initWithInteger:serviceID];
    FMResultSet *resultSet = [db executeQuery:query, servID];

    while ([resultSet next]) {
        NSDictionary *data = @{
                @"db_id"              : [NSNumber numberWithInt:[resultSet intForColumn:@"id"]],
                @"id"                 : [NSNumber numberWithInt:[resultSet intForColumn:@"feed_id"]],
                @"title"              : [resultSet stringForColumn:@"name"],
                @"unread"             : [NSNumber numberWithInt:[resultSet intForColumn:@"unread"]],
                @"last_updated"       : [NSNumber numberWithInt:[resultSet intForColumn:@"last_update"]],
                @"cat_id"             : [NSNumber numberWithInt:[resultSet intForColumn:@"category_id"]],
                @"is_category"        : [NSNumber numberWithInt:[resultSet intForColumn:@"is_category"]],
                @"has_icon"           : [NSNumber numberWithInt:[resultSet intForColumn:@"has_feed_icon"]],
                @"last_server_update" : [NSNumber numberWithInt:[resultSet intForColumn:@"last_server_update"]],
                @"order_id"           : [NSNumber numberWithInt:[resultSet intForColumn:@"order_id"]],
                @"article_sort_order" : [NSNumber numberWithInt:[resultSet intForColumn:@"article_sort_order"]],
                @"article_visibility" : [NSNumber numberWithInt:[resultSet intForColumn:@"article_visibility"]]
        };

        [array addObject:data];
    }

    return array;
}

// ---------------------------------------------------------------------------------------------------------------------

@end