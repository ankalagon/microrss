//
// Created by Grzesiek on 05.07.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@class TinyTinyRSSFeed;


@interface MicroRSSFeedTable : NSObject

/**
* Zapisuje do bazy danych feed TTRSS
*/
+(void)updateTTRSSFeed:(TinyTinyRSSFeed *)feed;

/**
* Zapisuje do bazy danych wszystkie feedy przekazane w parametrze
*
* @param feeds tablica obiektów TinyTinyRSSFeed
*/
+ (void)updateTTRSSFeeds:(NSArray *)feeds;

/**
* Usuwa feed o wskazanym ID z bazy
*/
+ (void)removeFeedFromDatabase:(NSInteger)feedDatabaseID;

/**
* Pobiera z bazy listę feed'ów zwracając je w formie tablicy słowników
*
* @return tablica zawierająca obiekty typu NSDictionary
*/
+ (NSArray *)fetchTTRSSFeedsForService:(NSInteger)serviceID;
@end