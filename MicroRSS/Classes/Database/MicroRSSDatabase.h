//
// Created by Grzesiek on 14.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface MicroRSSDatabase : NSObject

/**
* Zwraca ścieżkę do bazy danych
*/
+ (NSURL *)databasePath;

/**
* Zwraca ścieżkę do bazy w formie string'a
*/
+ (NSString *)databasePathString;

@end